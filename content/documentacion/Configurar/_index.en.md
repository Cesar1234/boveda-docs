---
title: "Configurar"
linkTitle: "Configurar"
weight: 2
icon: "fa fa-cogs fa-3x"
description: "En esta sección se podra configurar la cuenta, accesos, Buzón inteligente, notificaciónes, correos y certificados de su cuenta SAIT Bóveda ."
type : "docs"

---


![IMG](confi.png) 

 > **Siga los enlaces para entrar en detalle con la sección de configuración.**
