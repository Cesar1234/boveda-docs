---
title: "Clientes y Provedores"
linkTitle: "Clientes y Provedores"
weight: 6
icon: "fa fa-users fa-3x"
description: "En esta sección se encontraran las facturas emitidas o recibidas en estado vigentes, creditos, cancelados, pendientes, alterados e invalidos."
type : "docs"
---
{{< notice note >}}
 Los catálogos de Clientes (Emitidos) y Proveedores (Recibidos) son iguales, por lo que en este ejemplo se explica sólo el de Provedores (Recibidos).
{{< /notice>}}

Para acceder a la sección **Provedores** de **SAIT bóveda** dar clic en el ícono de **Provedores**.

![IMG](inicio.png)


>Siga los enlaces para entrar en detalle con la sección de **Clientes y Provedores**.


