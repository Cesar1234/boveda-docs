---
title: "Buzón"
linkTitle: "Buzón"
weight: 7
icon: "fa fa-inbox fa-3x"
description: "En esta sección se podran aceptar o rechazar las solicitudes de cancelación que se tengan pendientes, así como revisar el historial de todas las solicitudes que se han aceptado o rechazado."
type : "docs"
---

Para acceder a la sección **Buzón** de **SAIT bóveda** dar clic en el ícono de **Buzón**.

![IMG](inicio.png)


>Siga los enlaces para entrar en detalle con la sección de **Buzón**.



