---
title: "Descargas"
linkTitle: "Descargas"
weight: 8
icon: "fa fa-download fa-3x"
description: "En esta sección se podran descargar CFD's que se encuentren en el SAT mediante una solicitud."
type : "docs"
---

![IMG](inicio.png) 

{{< notice info>}}
En esta pestaña se podran descargar CFD's que se encuentren en el SAT mediante una solicitud, de tal manera que las facturas que se encuentren en el SAT en base a la solicitud realizada, se descargaran directamente a su cuenta de SAIT Bóveda. 
{{< /notice >}}


{{< notice note >}}
 Se podran descargar hasta 200 mil CFDI's del SAT por solicitud, con la condición de que las fechas no sean las mismas para cada petición.
{{</ notice >}}


### ° Información de descargas solicitadas.
En la tabla inferior se mostraran todos lo datos acerca de las descargas que se solicitaron al SAT, indicando toda la información de dichas descargas.

![IMG](datos.png) 


* **Origen:** El origen de donde surguieron las descargas realizadas.
* **CFDI's:** Tipo de facturas que se solicitaron a descargar (Emitidos o Recibidos).
* **Desde-Hasta:** el rango de fechas en que se mando a solicitar las descagas de las facturas en el SAT.
* **Cantidad:** Es la cantidad de facturas que se descargaron al realizar la solicitud.
* **Estado:** El estado actual en el que se encuentran las solicitudes que se mandaron al SAT, esta tiene los siguientes estados:

  * **Aceptada:** La solicitud está siendo procesada ante el SAT, esta tardara 30 minutos en verificar si la solicitud que fue pedida se encuentra en el SAT.
  * **Terminada:** La solicitud realizada ya fue verificada ante el SAT, descargando en la sección de **Proveedores(recibidos)** las facturas solicitadas.
  * **Vencida:** Las solicitudes realizadas ante el SAT no fueron encontradas, por lo cual no se pudo realizar ninguna descarga.


* **Solicitada:** La fecha en la que se solicito la descarga de la(s) factura(s).
* **Pendiente:** Estado en el que se encuetra la solicitud.
  * **Si:** Se encuentra en un estado de verificación ante el SAT, por lo tanto aun sigue pendiente la solicitud.
  * **No:** Ya se realizo el estado de verificación de la solicitud, por lo tanto ya no esta en estado pendiente dicha solicitud.

### ° Descargas SAT Disponibles.

Se mostrara las **Descargas SAT Disponibles**, esto indicando las descargas disponibles que se pueden hacer al SAT.

{{< notice info>}}
La cantidad de descargas disponibles dependera del paquete de Bóveda SAIT que se haya adquirido.
{{< /notice >}}

![IMG](cantidad.png)

### ° Agregar nueva solicitud.

Para agregar una solicitud de descarga al SAT, dar **clic** al boton de **Agregar**.

![IMG](agregar.png)

Se mostrara una ventana en donde se tendra que elegir la **Fecha inicial** y la **Fecha final**, tambien se tendra que elegir si las facturas a descargar seran **Emitidas o** **Recibidas.** 

![IMG](nueva1.png)

ya una vez completado dichos campos, dar **clic** en el boton de **Aceptar.** de esta se mandara una nueva solicitud de descarga con los datos solicitados anteriormente.

![IMG](nueva2.png)

Se mostrara una ventana de confirmación en donde se mostrara un aviso de que el **SAT es el que se encarga de ACEPTAR o RECHAZAR las solicitudes** y mostrando en la parte inferior el rango de fechas y el tipo de comprobante que se solicitara ante el SAT.
Ya una vez verificado que todo esta bien, dar **clic** al boton de **Aceptar.**

![IMG](confirmar.png)

Ya una vez confirmado la solicitud, se mostrara una notificacion avisando que **Se ha enviado una solicitud de descarga al SAT.**

![IMG](confirmar2.png)

De esta manera la solicitud realizada estara en **Estado aceptada**, indicando que esta pendiente dicha solicitud. Ya solo sera cuestion de que el SAT termine su verificación de validación.

{{< notice note>}}
 **Cada 30 minutos de manera automatica se realiza una nueva solicitud de descargas pendientes al SAT.**
{{< /notice>}}


![IMG](aceptada.png)

### ° Verificar solicitudes pendientes.

Para acelerar el proceso de las solicitudes pendientes y realizar la solicitud de manera inmediata, dar clic al boton de **Verificar solicitudes pendientes.**

![IMG](acelerar.png)

Aparecera una notificación indicando que las **Descargas verificadas fueron exitosamente realizadas.**

![IMG](notificacion.png)


De esta manera la solicitud pasara al estado **Terminada** o **Vencida** segun sea el caso de validación que haya tomado el SAT ante la solicitud solicitada. En este caso la solicitud su estado fue **Terminada** con un cantidad de **68 facturas descargadas.** 

![IMG](terminada.png)

{{< notice note>}}
Todas las facturas solicitadas que se hayan descargado, estaran en la sección de Proveedores (Recibidos).
{{< notice info>}}

![IMG](proveedores.png)

### ° Realizar busquedas de solicitudes.

Se podran realizar una busqueda de todas las solicitudes que estan y no estan en estado pendientes, para ello dar **clic** al campo de **Pendiente.** Se desplegara las siguientes opciones:
* **Todas:** Se muestran todas las solicitudes, pendientes y no pendientes.
* **Si:** Se muestran solo las solicitudes que su estado aun esta pendiente.
* **No:** Se muestran solo las solicitudes que su estado ya no esta pendiente, esto quiere decir que ya paso a un estado **Vencido** o **Terminado** segun el caso.

![IMG](pendiente.png)
