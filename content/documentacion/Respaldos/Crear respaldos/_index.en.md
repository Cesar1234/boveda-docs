---
title: "Crear respaldos"
linkTitle: "Crear respaldos"
weight: 3
description: "En esta pestaña se podran crear respaldos de cualquier archivo deseado."

---

### ° Descargar software.

{{< notice note>}}
Para crear respaldos en SAIT Bóveda se debera intalar un software que le permitira realizar dichos respaldos directamente en su cuenta de SAIT Bóveda.
{{< /notice >}}




>Para descargar el software de respaldos, dar clic al siguiente enlace.
[Descargar aqui](https://www.google.com/url?q=https://sait.mx/download/saitbkup.exe&sa=D&source=hangouts&ust=1578601526222000&usg=AFQjCNFcfhIBDdMommAxt-8bRDZs7yJshw)



### ° Instalación del software.

Se descargara un ejecutable en el directorio de descargas, para ejecutarlo dar doble **clic** en el ejecutable.

![IMG](descarga.png) 

Se mostrar una ventana de confirmación, dar **clic** en **ejecutar,** de esta manera se ejecutara el software y comenzará el proceso de instalación.

![IMG](confirmar.png) 

Se monstar la ventana de **Acuerdo de licencia**, el ya una vez que se haya leído los terminos y condiciones del programa, dar clic en **Acepto el acuerdo** y a continuación dar clic al boton de **Siguiente.**

![IMG](licencia.png) 

Se mostara una ventana indicando que ya todo está listo para instalación del software, para continuar con la instalación dar clic en el boton **instalar.**

![IMG](instalar.png) 


Ya una vez completado la instalación del software, se monstrar una ventana de finalizar, dar **clic** en el boton de **Finalizar.**

![IMG](finalizado.png) 

### ° Establecer cuenta.

Ya una vez completado la descarga, se abrira una pestaña en el navegador, en la cual se pedira ingresar una contraseña para poder acceder.

![IMG](inicio1.png) 

Ya una vez introducido la contraseña, dar clic al boton de **Guardar.**

![IMG](inicio2.png) 

Se abrira una ventana en donde le dara la bienvenida a la **configuración automatica de respaldos de SAIT.**, en la cual tendra que ingresar la contraseña que ingreso anteriormente.

![IMG](inicio3.png) 

Ya una vez ingresado la contraseña, dar **clic** al boton de **iniciar sesión.**

![IMG](inicio4.png) 

Ya una vez iniciado sesión, se abrira la sección de **Configuración de respaldos automaticos**, en donde por defecto se encontara en el apartado de **Respaldos programados**. En este apartado se mostraran todos los respaldos automaticos programados.

![IMG](opciones.png) 

### ° Agregar nueva cuenta.

Para programar un nuevo respaldo, **se debera tener agregada en esta sección su cuenta o alguna cuenta de SAIT Bóveda existente,** para agregar su cuenta o alguna cuenta existente en esta sección, dirigirse al apartado de cuentas.

![IMG](cuentas.png) 


Dentro de este apartado se encontraran todas las cuentas registradas, para agregar un nueva cuenta, dar clic al boton de **Agregar cuenta.**

![IMG](agregar_cuenta.png) 

Se monstrar una ventana en donde se tendra que insertar La llave **(Apikey)** de su cuenta SAIT Bóveda.

{{< notice info>}}
**La llave de la cuenta (Apikey) es usada para hacer una conexión segura con el servicio de SAIT Bóveda.**
{{< /notice>}}

![IMG](apikey.png) 


{{< notice info >}}
El **Apikey** se encuenta dentro de su cuenta SAIT Bóveda en la sección de **Configuración.**
{{< /notice>}}
![IMG](apikey_ubicacion.png) 

Ya una vez optenido e insertado el **Apikey**, dar **clic** en el boton de **Guardar.**

![IMG](key_si.png) 

Ya una vez guardada la cuenta, se mostraran los datos de dicha cuenta en la tabla inferior, mostrando su **RFC, Nombre y su estatus actual de la cuenta proporcionada**

![IMG](datos.png) 


Ya teniendo una cueta registrada se podra programar los resplados, para ella dirigirse al apartado de **Respaldos programados** y dar clic al boton de **Programar nuevo respaldo.**

![IMG](pro_respaldos.png) 

Se mostrara una ventana en donde se tendran que rellenar los siguientes campos:

* **1. Cuenta de SAIT Bóveda:** Seleccionar la cuenta en la que se realizaran y guardaran los respaldos.

* **2. Nombre de resplado:** Ingresar el nombre que tendra el respaldo.

* **3. Directorio a respaldar:** Se tendra que ingresar la dirección del directorio a respaldar, una forma facil de saber la direccion del directorio es dar un clic en la parte superior dentro de dicho directorio y seleccionar la ruta completa que se habilita.

![IMG](ruta.png) 


* **4. Realizar respaldo a las:** Es la hora en la que comenzara a realizar el respaldo, procure programar el respaldo a una hora en la que ya no se este utilizando el sistema, ya que se puede realentizar el sistema debido al proceso del respaldo.

Ya una vez rellenado todos los campos necesarios para la programación del respaldo, dar **clic** al boton de **Guardar.**

![IMG](respaldo.png) 

{{< notice note >}}
 El limite de archivos subidos es de 15 directorios, si se desea modificar el limite de archivos subidos, contactar con nosotros al correo **soporte@sait.mx.**
{{< /notice >}}

{{< notice info>}}
Ya una vez guardado el respaldo se encontara en el apartado de **Respaldos programados**, en donde se podra visualizar el respaldo creado anteriormente. 

{{< /notice >}}

![IMG](respaldo2.png) 

El respaldo tendra un **Estatus pendiente** ya que se realizara el **Respaldo** hasta que se cumpla con la hora que se le asignó al respaldo. Para realizar el respaldo de manera inmediata, dar clic al boton de **Realizar ahora**, de esa manera el respaldo comenzara imediatamente.

![IMG](respaldo3.png) 

De esa manera el resplado se comenzara a realizar asta llegar al **Estatus de completo**, indicando que se termino el respaldo de manera exitosa.

![IMG](respaldo4.png) 
![IMG](respaldo5.png) 

### ° información de respaldos.

Para ver a detalle la información del respaldo deseado, dar **clic** en el nombre del respaldo a visualizar.

![IMG](nombre.png) 

Se visualizarán todos los datos a detalle de dicho respaldo seleccionado montrando:

* **Fecha de ultimo respaldo:** Es la fecha y la hora en la que se realizo el ultimo respaldo.

* **Fecha del siguiente respaldo:** Es la fecha y la hora en la que se ejecutara el siguiente respaldo.

* **Cuenta:** Es la cuenta en la que se estan generando y guardando los respaldos.

![IMG](detalles.png)

### ° Eventos.


En el apartado de eventos se econtrara todo el historial de los eventos realizados en el momento que se realizo el respaldo.

![IMG](eventos.png)



