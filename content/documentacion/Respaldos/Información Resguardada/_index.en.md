---
title: "Información Resguardada"
linkTitle: "Información Resguardada"
weight: 1
description: "en esta pestaña se encontara toda la información que se resguardo mediante los respaldos generados."

---


![IMG](inicio.png) 

### ° Detalles de respaldo

Para ver los detalles de cualquier respaldo, **dar clic al nombre del respaldo** a visualizar o al enlace de **Ver detalle.**

![IMG](detalles.png) 

**Se mostraran los detalles del respaldo seleccionado monstrando la siguiente información:**


* **UUID:** Es el identificador unico que se le tiene asignado al respaldo.

* **Checksum:** Es una manera de proteger su información resguardada de cualquier cambio accidental o cualquier cambio de terceros hacia su información, de tal manera que si alguna persona descarga su información que se tiene resguardada aquí en SAIT Bóveda y la modifica a su veneficio gracias al 

* **Checksum** se dara cuenta que información fue alterada.

* **contraseña ZIP:** Es la contraseña que se le asigna a su **archivo.zip** para poder descomprimido. De tal manera que no cualquier persona pueda descomprimir la información que se encuentra en los respaldos descargados.

![IMG](info.png) 

### ° Descargar respaldos

Para descargar algun respaldo, dar **clic** al enlace de **Descargar** en el respaldo deseado.

![IMG](guardar.png) 

### ° Descomprimir archivo.zip

Al completar la descarga, se abra descargado un **arichivo.zip**, esto quiere decir que su respaldo se encontrara comprimido y protegido.

![IMG](zip.png) 

ya una vez ubicado su respaldo comprimido, se tendra que **descomprimir** para poder tener acceso a la información.

![IMG](archivo.png) 

Para ello dar clic derecho sobre el archivo, se mostrara un pequeño menú en donde se tendra que dar un clic a la opción de **Extraer en** y en seguida se mostrara el nombre de su directorio en donde se decomprimira el archivo.

![IMG](extraer.png) 

{{< notice note >}}
 tambien se podra descomprimir el archivo dando clic derecho sobre el archivo y dar un clic en la opción extraer aquí, de esa manera tambien de decomprimira el archivo de igual manera.**
{{< /notice >}}

![IMG](extraer2.png) 

Se mostrara una ventana en donde se le pedira la contraseña para poder descomprimir dicho respaldo.

![IMG](pass.png) 

{{< notice note>}}
La contraseña requerida se encuentra en los detalles del respaldo descargado.
![IMG](pass2.png) 

{{< /notice>}}

Ya una vez ingresada la contraseña requirida, dar clic en el boton de **Aceptar.**

![IMG](pass3.png) 

De esa manera el archivo comenzara a descomprimirse. **Ya una vez descomprimido estara toda la información de su respaldo en el mismo directorio en donde estaba su archivo comprimido.**

![IMG](completo.png) 

