---
title: "Respaldos programados"
linkTitle: "Respaldos programados"
weight: 2
description: "En esta pestaña se encontraran todos los respaldos activos e incativos programados."
---


![IMG](inicio.png) 

* **ID:** Es el identificador unico del respaldo programado.

* **Nombre:** Es el nombre que se le asigno al respaldo.

* **Activo:** Es el estado en el que se encuentra el respaldo.
  * **Activo:** aun se encuentra programado para continuar realizando la descarga programada.
 
  * **inactivo:** Ya no se encuentra programado, por lo tanto ya no se descargara diho respaldo de manera automatica.



Para poner inactivo algun respaldo deseado, dar clic al boton de **Avanzado**

![IMG](avanzado.png) 

Se monstrara una ventana en donde se podra activar o desctivar el estado del respaldo, **para activar el resplado solo activar la casilla, de lo contrario descativarla.**

![IMG](casilla.png) 